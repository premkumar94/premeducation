// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA3GGrSg1HEd0O1TQ5PL0PuYjlj33-zoz8",
    authDomain: "premeducation-md94.firebaseapp.com",
    databaseURL: "https://premeducation-md94.firebaseio.com",
    projectId: "premeducation-md94",
    storageBucket: "premeducation-md94.appspot.com",
    messagingSenderId: "405741921600"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
