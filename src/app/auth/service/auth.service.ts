import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private fb: AngularFireAuth) { }
  checkLogin(){
    console.log(this.fb.authState);
  }
}
