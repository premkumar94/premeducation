import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
export interface DialogData {
  
}
@Component({
  selector: 'app-profile-login',
  templateUrl: './profile-login.component.html',
  styles: []
})
export class ProfileLoginComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ProfileLoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
    email = new FormControl('', [Validators.required, Validators.email]);
    password = new FormControl('', [Validators.required]);
  
    getErrorMessage() {
      return this.email.hasError('required') ? 'You must enter a value' :
          this.email.hasError('email') ? 'Not a valid email' :
              '';
    }
    
  
  ngOnInit() {
    
  }

}
