import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MySkillComponent } from './my-skill.component';

const routes: Routes = [
  {
    path: '',
    component: MySkillComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MySkillRoutingModule { }
