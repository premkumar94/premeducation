import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { routerTransition } from '../../router.animations';
@Component({
  selector: 'app-my-skill',
  templateUrl: './my-skill.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class MySkillComponent implements OnInit {
  myskils:Observable<any>;
  chartData;
  loader: boolean = true; 
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  tooltipDisabled = false;
  xAxisLabel = 'Tools';
  mAxisLabel = 'Month';
  showYAxisLabel = true;
  yAxisLabel = 'percentage';
  showGridLines = true;
  innerPadding = 0;
  autoScale = true;
  timeline = false;
  barPadding = 15;
  groupPadding = 0;
  roundDomains = false;
  maxRadius = 10;
  minRadius = 3;
  showLabels = true;
  explodeSlices = false;
  doughnut = false;
  arcWidth = 0.25;
  rangeFillOpacity = 0.15;
  legendPosition = 'right';
  colorScheme = {
    domain: ['#4fc3f7', '#fb8c00', '#A10A28', '#C7B42C', '#5AA454', '#7460ee', '#f62d51', '#20c997', '#2962FF', '#AAAAAA']
  };
  schemeType = 'ordinal';
  constructor(public af: AngularFireDatabase) {
    this.myskils = af.list('myskill/ourskill').valueChanges();
    this.myskils.subscribe((res) => 
    {
      this.loader = false;
      this.chartData = res;
    });


   }
  
  ngOnInit() {
  }

}
