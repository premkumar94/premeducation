import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MySkillRoutingModule } from './my-skill-routing.module';
import { MySkillComponent } from './my-skill.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../../../environments/environment';
import { NgxChartsModule } from '@swimlane/ngx-charts';
@NgModule({
  imports: [
    CommonModule,
    MySkillRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    NgxChartsModule
  ],
  declarations: [MySkillComponent]
})
export class MySkillModule { }
