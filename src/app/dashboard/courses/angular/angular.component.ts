import { Component, OnInit } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { YouPlayListService } from './you-play-list.service';
import { MatDialog } from '@angular/material';
import { PopupComponent } from './popup/popup.component';
import { routerTransition } from '../../../router.animations';
@Component({
  selector: 'app-angular',
  templateUrl: './angular.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class AngularComponent implements OnInit {

  constructor(private ypl: YouPlayListService, public dialog: MatDialog) { }
  openDialog(item): void {
    const dialogRef = this.dialog.open(PopupComponent, {
      width: '540px',
      data: { item}
      // name: this.name, animal: this.animal
    });

    dialogRef.afterClosed().subscribe(result => {
      
      
    });
  }
  

  private id: string;
  
  videoList: any;
  loader: boolean = true; 
  ngOnInit() {
    this.ypl.getYoutubePlayList().subscribe(
      res => {
        this.videoList = res,
        this.loader = false
      }
    )
  }

}
