import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class YouPlayListService {

  constructor(private http: HttpClient) { }
  getYoutubePlayList() {
   return this.http.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=25&playlistId=PLKOCU4eAiXG11bLFMLCc_PvGvTlc8x5CC&key=AIzaSyDXSO-W3Mtk2VVz6lHkppvrBqvZr9q_Ifg")
  }

}
