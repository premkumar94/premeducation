import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styles: []
})
export class PopupComponent implements OnInit {
  videoId;
  constructor(
    public dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      this.videoId = this.data;
      
    }
    player: YT.Player;
    savePlayer(player) {
      this.player = player;
      
    }
    onStateChange(event) {
      
    }
    
    
  ngOnInit() {
  }

}
