import { TestBed, inject } from '@angular/core/testing';

import { YouPlayListService } from './you-play-list.service';

describe('YouPlayListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YouPlayListService]
    });
  });

  it('should be created', inject([YouPlayListService], (service: YouPlayListService) => {
    expect(service).toBeTruthy();
  }));
});
