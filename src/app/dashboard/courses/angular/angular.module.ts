import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YoutubePlayerModule } from 'ng2-youtube-player';
import { AngularRoutingModule } from './angular-routing.module';
import { AngularComponent } from './angular.component';
import { YouPlayListService } from './you-play-list.service';
import { PopupComponent } from './popup/popup.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    AngularRoutingModule,
    YoutubePlayerModule,
    MatDialogModule,
    MatButtonModule
  ],
  declarations: [AngularComponent, PopupComponent],
  providers: [YouPlayListService],
  entryComponents: [
    PopupComponent
  ]
})
export class AngularModule { }
