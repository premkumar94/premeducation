import { Component, OnInit } from '@angular/core';
import { YoutubeApiService } from './youtube-api.service';
import { routerTransition } from '../../router.animations';
@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class CoursesComponent implements OnInit {

  constructor(public getApi: YoutubeApiService) { }
  playlist: any;
  loader: boolean = true; 
  ngOnInit() {
    this.getApi.getYouList().subscribe(
      res => {
        this.playlist = res,
        this.loader = false
      }
    )
  }

}
