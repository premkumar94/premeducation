import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesComponent } from './courses.component';
import { YoutubeApiService } from './youtube-api.service';
import { TrimPipe } from './trim.pipe';

@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule,
    HttpClientModule
  ],
  declarations: [CoursesComponent, TrimPipe],
  providers: [YoutubeApiService]
})
export class CoursesModule { }
