import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
 
import { environment } from '../../../environments/environment';
@NgModule({
  imports: [
    CommonModule,
    AboutRoutingModule,
    MatCardModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  declarations: [AboutComponent]
})
export class AboutModule { }
