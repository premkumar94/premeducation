import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { routerTransition } from '../../router.animations';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class AboutComponent implements OnInit {
  aboutme:Observable<any>;
  loader: boolean = true; 
  about_detail:Observable<any>;
  constructor(public af: AngularFireDatabase) {
    this.aboutme = af.list('about/aboutme').valueChanges();
    this.about_detail = af.list('about/about_detail').valueChanges();
    this.aboutme.subscribe(() => this.loader = false);
    this.about_detail.subscribe(() => this.loader = false);
   }
  
  
  ngOnInit() {
  }

}
