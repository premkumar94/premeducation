import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../youtube.service';
import { routerTransition } from '../../router.animations';
@Component({
  selector: 'app-my-video',
  templateUrl: './my-video.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class MyVideoComponent implements OnInit {
  myinfo;
  constructor(private mi: YoutubeService) { }
  public number: number;
  public config = {
    animation: 'count', 
    format: 'd', 
    theme: 'car', 
    value: 50,
    auto: true,
}
  ngOnInit() {
    this.mi.myChennal().subscribe(res => 
    {
      this.myinfo = res,
      console.log(res)
    }
    )
  }

}
