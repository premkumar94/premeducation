import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyVideoRoutingModule } from './my-video-routing.module';
import { MyVideoComponent } from './my-video.component';
import { MatCardModule } from '@angular/material/card';
import { Ng2OdometerModule } from 'ng2-odometer';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MyVideoRoutingModule,
    Ng2OdometerModule.forRoot() 

  ],
  declarations: [MyVideoComponent]
})
export class MyVideoModule { }
