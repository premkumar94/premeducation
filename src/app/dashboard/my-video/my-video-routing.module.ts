import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyVideoComponent } from './my-video.component';

const routes: Routes = [
  {
    path: '',
    component: MyVideoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyVideoRoutingModule { }
