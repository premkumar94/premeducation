import { Component, Inject } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ProfileLoginComponent } from './profile-login/profile-login.component';
export interface DialogData {

}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  fullBody = document.querySelector('body');
  full() {
    this.fullBody.classList.add('full');
  }
  fullMinus() {
    this.fullBody.classList.remove('full');
  }
  constructor(private breakpointObserver: BreakpointObserver, public dialog: MatDialog) { }
  openDialog(): void {
    const dialogRef = this.dialog.open(ProfileLoginComponent, {
      width: '400px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

}
