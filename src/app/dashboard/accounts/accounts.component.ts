import { Component, OnInit } from '@angular/core';
import { routerTransition } from 'src/app/router.animations';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class AccountsComponent implements OnInit {
  rows:Observable<any>;
  tableData;
  loadingIndicator;
  constructor(public af: AngularFireDatabase) {
    this.rows = af.list('accounts/expense').valueChanges();
    this.rows.subscribe((res) => 
    {
      this.loadingIndicator = false;
      this.tableData = res;
    });


   }
  ngOnInit() {
  }

}
