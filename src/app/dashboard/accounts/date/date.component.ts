import { Component, OnInit } from '@angular/core';
import { routerTransition } from 'src/app/router.animations';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styles: [],
  animations: [routerTransition()]
})
export class DateComponent implements OnInit {
  months:Observable<any>;
  monthsData;
  loader;
  monthId;
  constructor(public af: AngularFireDatabase, private route: ActivatedRoute) {
    this.route.params.subscribe(
      (params: any) => {
        this.monthId = params.id
      }
    )
    this.months = af.list('/accounts/date/' + this.monthId).valueChanges();
    this.months.subscribe((res) => 
    {
      this.loader = false;
      this.monthsData = res;
    });


   }
  ngOnInit() {
   
  }

}
