import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { AccountsComponent } from './accounts.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DateComponent } from './date/date.component';

@NgModule({
  imports: [
    CommonModule,
    AccountsRoutingModule,
    NgxDatatableModule
  ],
  declarations: [AccountsComponent, DateComponent]
})
export class AccountsModule { }
