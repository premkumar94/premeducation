import { Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  constructor(private el: ElementRef) {

  }
  title = 'premeducation';
  dark: boolean = false;
  showSettings;
  change(dark) {
    let myTag = document.querySelector("body")
    if (this.dark == true) {
      
      myTag.classList.add('dark');
      this.showSettings = false;
    }
    else {
      myTag.classList.remove('dark');
      this.showSettings = false;
    }
  }
  
}
